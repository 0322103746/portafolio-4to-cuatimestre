import { useEffect, useState } from "react";
import { StyleSheet, View, Text, ActivityIndicator, FlatList } from "react-native";

const url = 'https://jsonplaceholder.typicode.com/posts';

export default function Post(){

    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(response => {
                setIsLoading(false);
                setData(response);
            }, error => {
                setIsLoading(false);
                setError(error);
            })
    }, []);


    const getContent = () => {
        if(isLoading) {
          return (
            <View>
              <Text style={styles.textProps}>Loading Data ...</Text>
              <ActivityIndicator size="large" color="green" /> 
            </View>
          );
        }
        if(error) {
            return <Text>{error}</Text>
        }

        return (
            <View>
                <FlatList 
                    data={data}
                    renderItem={({item}) => (
                        <View>
                            <Text>{item.title}</Text>
                        </View>
                    )}
                />
            </View>
        );
    }

    console.log(data);


    return (
        <View>
          {getContent()}
        </View>
    );
}

const styles = StyleSheet.create({
    textProps: {
        fontSize: 32,
    },
});