
import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';

import Button from './components/Button';
import ImageViewer from './components/ImageViewer';

import * as ImagePicker from 'expo-image-picker';

import IconButton from './components/IconButton';
import CircleButtons from './components/CircleButton';
import EmojiPicker from './components/EmojiPicker';

// -------------------------------------------------------------------------- //

const url = 'http://api.coindesk.com/v1/bpi/currentprice.json';

const PlaceholderImage = require("./assets/perro.png");

export default function App() {
  // El set es el que puede declarar el primer termino que es privado
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [showAppOptions, setShowAppOptions] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  const PickImageAsync = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 1,
    });

    if (!result.canceled) {
      setSelectedImage(result.assets[0].uri);
      setShowAppOptions(true);
    } else {
      alert("No has seleccionado ninguna imagen.");
    }
  }

  const onReset = () => {
    setShowAppOptions(false);
  }

  const onAddSticker = () => {
    setIsModalVisible(true);
  }

  const onSaveImageAsync = async () => {
  }

  const onModalClose = () => {
    setIsModalVisible(false);
  }

  // ------------------------------------------------------------------------ //

  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <ImageViewer selectedImage={selectedImage} PlaceholderImageSource={PlaceholderImage} />
      </View>

      {showAppOptions ? (
        <View style={styles.optionsContainer}>
          <View style={styles.optionsRow}>
            <IconButton icon="refresh" label="Reiniciar" onPress={onReset}/>
            <CircleButtons onPress={onAddSticker}/>
            <IconButton icon="save-alt" label="Guardar" onPress={() => setShowAppOptions(false)}/>
          </View>
        </View>

      ) : (

        <View style={styles.optionsContainer}>
          <Button onPress={PickImageAsync} theme="primary" label="Selecciona una imagen"/>
        </View>
      )}

      <EmojiPicker isVisible={isModalVisible} onClose={onModalClose}>

      </EmojiPicker>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor: '#1c1c1e',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image : {
    width : '100%',
    width: '100%',
    top : -100,
    alignItems : "center",

  },
  optionsContainer : {
    position : "absolute",
    bottom : 80,
  },
  optionsRow : {
    alignItems : "center",
    flexDirection : "row",
  },
});

// -------------------------------------------------------------------------- //