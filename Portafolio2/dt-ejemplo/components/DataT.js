import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';

const DataT = () => {
    const [data, setData] = useState([]);

    const fetchData = async () => {
        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts');
            const data = await response.json();
            setData(data);

        } catch (error) {
            console.error('Ha ocurrido un error al obtener los datos: ', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const columns = [
        {
            name: 'ID',
            selector: row => row.id,
        },
        {
            name: 'Usuario (ID)',
            selector: row => row.userId,
        },
        {
            name: 'TÃ­tulo',
            selector: row => row.title,
        },
        {
            name: 'Cuerpo',
            selector: row => row.body,
        },
    ];

    return (
        <DataTable title="POSTS" columns={columns} data={data} />
    );
};

export default DataT;